package com.example.grouper;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import java.util.EventListener;
import java.util.HashMap;
import java.util.Map;

public class EventsActivity extends AppCompatActivity {


    EventsAdapter eventsAdapter;
    Context c;
    ListView eventsList;
    Map<String, Object> eventsMap = new HashMap<String, Object>();
    TextView emptyEvents;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);

        Resources res = getResources();
        eventsList = findViewById(R.id.events_list);
        emptyEvents = findViewById(R.id.empty_events);
        emptyEvents.setText("");
        c = this;
        GetEvents getEvents = new GetEvents();
        getEvents.execute("");


    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.addEvent:
                startActivity(new Intent(EventsActivity.this, AddEvent.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.events_menu, menu);
        return true;
    }

    private class GetEvents extends AsyncTask<String, String, String>{

        DatabaseHelper db;


        @SuppressLint("WrongThread")
        @Override
        protected String doInBackground(String... strings) {
            db = new DatabaseHelper(c);
            eventsMap = db.getEvents();
            if (eventsMap.isEmpty()){
                emptyEvents.setText("Brak aktualnych wydarzeń");
            } else {
                emptyEvents.setText("");
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (eventsMap.size() > 0){
                eventsAdapter = new EventsAdapter(c, eventsMap);
                eventsList.setAdapter(eventsAdapter);
                eventsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent showEventsDetails = new Intent(EventsActivity.this, EventDetail.class);
                        showEventsDetails.putExtra("com.example.EVENT_ID", id);
                        startActivity(showEventsDetails);
                    }
                });
            }
        }
    }


}
