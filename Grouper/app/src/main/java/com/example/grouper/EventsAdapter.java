package com.example.grouper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class EventsAdapter extends BaseAdapter {

    LayoutInflater mInflater;
    Map<String, Object> map;
    List<String> ids;
    List<Object> events;

    EventsAdapter(Context c, Map m){
        mInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        map = m;
        ids = new ArrayList<String>(map.keySet());
        events = new ArrayList<Object>(map.values());
    }

    @Override
    public int getCount() {
        return map.size();
    }

    @Override
    public Object getItem(int position) {
        return events.get(position);
    }

    @Override
    public long getItemId(int position) {
        return Long.parseLong(ids.get(position));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = mInflater.inflate(R.layout.event_layout, null);
        TextView dateText = v.findViewById(R.id.dateText);
        TextView placeText = v.findViewById(R.id.placeText);
        TextView descriptionText = v.findViewById(R.id.descriptionText);
        Map<String, Object> event = (Map<String, Object>) events.get(position);
        dateText.setText(((String)event.get("date")));
        placeText.setText((String)event.get("place"));
        if (event.get("groupname")!=null) {
            descriptionText.setText((String) event.get("groupname") + ": " + (String) event.get("description"));
        } else {
            descriptionText.setText((String) event.get("description"));
        }
        return v;
    }
}
