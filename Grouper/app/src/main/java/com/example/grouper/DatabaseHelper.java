package com.example.grouper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.HashMap;
import java.util.Map;

public class DatabaseHelper extends SQLiteOpenHelper{
    public static final String DATABASE_NAME ="grouper.db";
    public static final String TABLE_NAME_USER ="users";
    public static final String TABLE_NAME_EVENTS ="events";
    public static final String TABLE_NAME_GROUPS ="groups";
    public static final String TABLE_NAME_USERS_GROUPS ="users_groups";
    public static final String ID ="ID";
    public static final String USER_USERNAME ="username";
    public static final String USER_PASSWORD ="password";
    public static final String EVENT_PLACE = "place";
    public static final String EVENT_DATE = "date";
    public static final String EVENT_DESCRIPTION = "description";
    public static final String SEPARATOR = ", ";
    public static final String CURRENT_USER = "id_user";
    public static final String GROUP_NAME = "groupname";
    public static final String GROUP_ID = "id_group";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 7);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE users (ID INTEGER PRIMARY  KEY AUTOINCREMENT, username TEXT, password TEXT)");
        sqLiteDatabase.execSQL("CREATE TABLE events (ID INTEGER PRIMARY  KEY AUTOINCREMENT, place TEXT, date TEXT, description TEXT, id_user INTEGER, id_group INTEGER, foreign key (id_user) references users(ID), foreign key (id_group) references groups(ID))");
        sqLiteDatabase.execSQL("CREATE TABLE groups (ID INTEGER PRIMARY  KEY AUTOINCREMENT, groupname TEXT)");
        sqLiteDatabase.execSQL("CREATE TABLE users_groups (ID INTEGER PRIMARY  KEY AUTOINCREMENT, id_user INTEGER, id_group INTEGER, foreign key (id_user) references users(ID), foreign key (id_group) references users(ID))");
        sqLiteDatabase.execSQL("INSERT INTO groups(groupname) VALUES ('5 sem')");
        sqLiteDatabase.execSQL("INSERT INTO groups(groupname) VALUES ('KINo')");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(" DROP TABLE IF EXISTS " + TABLE_NAME_USER);
        sqLiteDatabase.execSQL(" DROP TABLE IF EXISTS " + TABLE_NAME_EVENTS);
        sqLiteDatabase.execSQL(" DROP TABLE IF EXISTS " + TABLE_NAME_GROUPS);
        sqLiteDatabase.execSQL(" DROP TABLE IF EXISTS " + TABLE_NAME_USERS_GROUPS);
        onCreate(sqLiteDatabase);
    }

    public long addUser(String user, String password){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(USER_USERNAME,user);
        contentValues.put(USER_PASSWORD,password);
        long res = db.insert(TABLE_NAME_USER,null,contentValues);
        db.close();
        return  res;
    }

    public boolean checkUser(String username, String password){
        String[] columns = { ID };
        SQLiteDatabase db = getReadableDatabase();
        String selection = USER_USERNAME + "=?" + " and " + USER_PASSWORD + "=?";
        String[] selectionArgs = { username, password };
        Cursor cursor = db.query(TABLE_NAME_USER,columns,selection,selectionArgs,null,null,null);
        int count = cursor.getCount();
        if(count>0){
            cursor.moveToFirst();
            GlobalValues.setUserId(cursor.getString(0));
        }
        cursor.close();
        db.close();
        if(count>0) {
            assignUserToCustGroups();
            return true;
        } else {
            return false;
        }
    }

    public long addEvent(String place, String date, String description, String groupId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(EVENT_PLACE,place);
        contentValues.put(EVENT_DATE,date);
        contentValues.put(EVENT_DESCRIPTION,description);
        contentValues.put(CURRENT_USER, GlobalValues.getUserId());
        contentValues.put(GROUP_ID, groupId);
        long res = db.insert(TABLE_NAME_EVENTS,null,contentValues);
        return res;
    }

    public Map<String, Object> getEvents(){
        Map<String, Object> map = new HashMap<String, Object>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT "+ ID + SEPARATOR + EVENT_PLACE+ SEPARATOR + EVENT_DATE + SEPARATOR + EVENT_DESCRIPTION + SEPARATOR + " (SELECT groupname FROM groups WHERE ID = events.id_group) FROM " + TABLE_NAME_EVENTS + " WHERE "+ CURRENT_USER + " = '" + GlobalValues.getUserId()+"' OR " + GROUP_ID + " IN (SELECT " + GROUP_ID +" FROM " + TABLE_NAME_USERS_GROUPS + " WHERE " + CURRENT_USER + " = '" + GlobalValues.getUserId()+"') ORDER BY " + EVENT_DATE, null);
        if (c.moveToFirst()){
            do {
                String id = c.getString(0);
                String place = c.getString(1);
                String date = c.getString(2);
                String description = c.getString(3);
                String groupname = c.getString(4);
                Map<String, Object> event = new HashMap<String, Object>();
                event.put(EVENT_PLACE, place);
                event.put(EVENT_DATE, date);
                event.put(EVENT_DESCRIPTION, description);
                event.put(GROUP_NAME, groupname);
                map.put(id, event);
            } while(c.moveToNext());
        }
        c.close();
        db.close();
        return map;
    }

    public Map<String, Object> getEventById(String eventId){
        Map<String, Object> map = new HashMap<String, Object>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT "+ ID + SEPARATOR + EVENT_PLACE+ SEPARATOR + EVENT_DATE + SEPARATOR + EVENT_DESCRIPTION + SEPARATOR + " (SELECT groupname FROM groups WHERE ID = events.id_group) FROM " + TABLE_NAME_EVENTS + " WHERE "+ ID + " = '" + eventId+"'", null);
        if (c.moveToFirst()){
            do {
                String id = c.getString(0);
                String place = c.getString(1);
                String date = c.getString(2);
                String description = c.getString(3);
                String groupname = c.getString(4);
                map.put(EVENT_PLACE, place);
                map.put(EVENT_DATE, date);
                map.put(EVENT_DESCRIPTION, description);
                map.put(GROUP_NAME, groupname);
            } while(c.moveToNext());
        }
        c.close();
        db.close();
        return map;
    }

    public boolean deleteEventById(String eventId){
        SQLiteDatabase db = this.getReadableDatabase();
        return db.delete(TABLE_NAME_EVENTS, ID + "=" + eventId, null) > 0;
    }

    public Map<String, Object> getGroupsForCurrentUser(){
        Map<String, Object> map = new HashMap<String, Object>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT "+ ID + SEPARATOR + GROUP_NAME + " FROM " + TABLE_NAME_GROUPS + " WHERE "+ ID + " IN (SELECT " + GROUP_ID + " FROM " + TABLE_NAME_USERS_GROUPS + " WHERE " + CURRENT_USER + " = '" + GlobalValues.getUserId()+"')", null);
        if (c.moveToFirst()){
            do {
                String id = c.getString(0);
                String groupName = c.getString(1);
                map.put(groupName, id);
            } while(c.moveToNext());
        }
        c.close();
        db.close();
        return map;
    }

    public void assignUserToCustGroups(){
        String[] columns = { ID };
        SQLiteDatabase db = getReadableDatabase();
        String selection = CURRENT_USER + "=?";
        String[] selectionArgs = { GlobalValues.getUserId() };
        Cursor cursor = db.query(TABLE_NAME_USERS_GROUPS,columns,selection,selectionArgs,null,null,null);
        int count = cursor.getCount();
        db.close();

        if (count==0) {
            db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(GROUP_ID, 1);
            contentValues.put(CURRENT_USER, GlobalValues.getUserId());
            db.insert(TABLE_NAME_USERS_GROUPS, null, contentValues);
            contentValues.put(GROUP_ID, 2);
            db.insert(TABLE_NAME_USERS_GROUPS, null, contentValues);
            db.close();
        }
    }
}
