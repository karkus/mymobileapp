package com.example.grouper;

import android.content.Context;
import android.content.Intent;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import java.util.Map;

public class EventDetail extends AppCompatActivity {

    DatabaseHelper db;
    TextView placeView;
    TextView dateView;
    TextView descriptionView;
    Button deleteEvent;
    private static Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getApplicationContext();
        setContentView(R.layout.event_detail);
        Intent in = getIntent();
        final Long eventId = in.getLongExtra("com.example.EVENT_ID", -1);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width*0.8), (int) (height*0.3));
        if (eventId == -1){
            Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
            Intent moveToEvents = new Intent(EventDetail.this,EventsActivity.class);
            startActivity(moveToEvents);
        } else {
            placeView = findViewById(R.id.event_detail_place);
            dateView = findViewById(R.id.event_detail_date);
            descriptionView = findViewById(R.id.event_detail_description);
            deleteEvent = findViewById(R.id.button_deleteEvent);
            db = new DatabaseHelper(context);
            Map<String, Object> event = db.getEventById(eventId.toString());
            placeView.setText((String) event.get(DatabaseHelper.EVENT_PLACE));
            dateView.setText((String) event.get(DatabaseHelper.EVENT_DATE));
            if (event.get(DatabaseHelper.GROUP_NAME)!=null) {
                descriptionView.setText((String) event.get(DatabaseHelper.GROUP_NAME) + ": " + (String) event.get(DatabaseHelper.EVENT_DESCRIPTION));
            } else {
                descriptionView.setText((String) event.get(DatabaseHelper.EVENT_DESCRIPTION));
            }
            deleteEvent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Boolean isDeleted = db.deleteEventById(eventId.toString());
                    if (isDeleted == false){
                        Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
                    } else {
                        Intent moveToEvents = new Intent(EventDetail.this,EventsActivity.class);
                        startActivity(moveToEvents);
                    }
                }
            });
        }

    }
}
