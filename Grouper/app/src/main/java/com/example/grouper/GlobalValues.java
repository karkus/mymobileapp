package com.example.grouper;

import android.app.Application;

public class GlobalValues extends Application {
    private static String userId;

    public static String getUserId() {
        return userId;
    }

    public static void setUserId(String newUserId){
        userId = newUserId;
    }

}
