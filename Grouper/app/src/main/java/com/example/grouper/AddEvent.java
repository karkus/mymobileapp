package com.example.grouper;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;

public class AddEvent extends Activity {

    DatabaseHelper db;
    TextView placeView;
    TextView dateView;
    TextView timeView;
    TextView descriptionView;
    Spinner groupsSpinner;
    RadioGroup radioGroup;
    RadioButton forMyself;
    Map<String, Object> groupsMap;
    private static Context context;
    private DatePickerDialog.OnDateSetListener dateListener;
    private TimePickerDialog.OnTimeSetListener timeListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getApplicationContext();
        setContentView(R.layout.add_event);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width*0.8), (int) (height*0.45));

        db = new DatabaseHelper(this);

        groupsMap = db.getGroupsForCurrentUser();

        ArrayAdapter<String> groupAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, new ArrayList<>(groupsMap.keySet()));
        groupAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        groupsSpinner = findViewById(R.id.spinnerGroup);
        groupsSpinner.setEnabled(false);
        groupsSpinner.setAdapter(groupAdapter);
        radioGroup = findViewById(R.id.radio_group);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.radioMyself){
                    groupsSpinner.setEnabled(false);
                } else {
                    groupsSpinner.setEnabled(true);
                }
            }
        });
        placeView = findViewById(R.id.edittext_place);
        dateView = findViewById(R.id.edittext_date);
        dateView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePicker = new DatePickerDialog(AddEvent.this, android.R.style.Theme_Holo_Light_Dialog_MinWidth, dateListener, year, month, day);
                datePicker.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                datePicker.show();
            }
        });
        dateListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;
                String date = year+"/"+month +"/"+dayOfMonth;
                dateView.setText(date);
            }
        };
        timeView = findViewById(R.id.edittext_time);
        timeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int hour = cal.get(Calendar.HOUR);
                int minute = cal.get(Calendar.MINUTE);
                TimePickerDialog timePicker = new TimePickerDialog(AddEvent.this,  timeListener, hour, minute, true);
                timePicker.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                timePicker.show();
            }
        });
        timeListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                String time = hourOfDay + ":" + minute;
                timeView.setText(time);
            }
        };
        forMyself = findViewById(R.id.radioMyself);
        descriptionView = findViewById(R.id.edittext_description);
        Button addEvent = findViewById(R.id.button_addEvent);
        addEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String place = placeView.getText().toString();
                String date = dateView.getText().toString();
                String time = timeView.getText().toString();
                String description = descriptionView.getText().toString();
                long res = 0;
                if (place == null || date.equals("Wybierz date") || time.equals("Wybierz godzine") || description == null ||  description == ""){
                    Toast.makeText(context, "Złe dane", Toast.LENGTH_SHORT).show();
                }else {
                    String finalDate = date + " " + time;
                    if (forMyself.isChecked()) {
                        res = db.addEvent(place, finalDate, description, null);
                    } else {
                        String groupId = (String) groupsMap.get(groupsSpinner.getSelectedItem().toString());
                        res = db.addEvent(place, finalDate, description, groupId);
                    }
                    Toast.makeText(context, "Dodano nowe wydarzenie", Toast.LENGTH_SHORT).show();
                    Intent moveToEvents = new Intent(AddEvent.this,EventsActivity.class);
                    startActivity(moveToEvents);
                }
            }
        });
    }
}
